<?php
require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("shaun");

echo "Name of Animal:  $sheep->name <br>"; // "shaun"
echo "Total Legs:  $sheep->legs <br>"; // 2
echo "Cold Blood Animal:  $sheep->cold_blooded <br><br>"; // false

// index.php
$sungokong = new Ape("kera sakti");
echo "Name of Animal:  $sungokong->name <br>";
echo "Total Legs:  $sungokong->legs <br>";
echo "Cold Blood Animal:  $sungokong->cold_blooded <br>";
$sungokong->yell();
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name of Animal:  $kodok->name <br>";
echo "Total Legs:  $kodok->legs <br>";
echo "Cold Blood Animal:  $kodok->cold_blooded <br>";
$kodok->jump() ;

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>